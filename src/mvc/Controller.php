<?php
/*  Controller
*   The controller object is the glue between
*   a View(template to render), and models(data).
*   The controller does the logic behind rendering
*   a view by working with models, then rendering
*   the view. You must implement the logic method yourself
*/

namespace CosmicFramework\MVC;

use CosmicFramework\MVC\View;

class Controller {
    // The view object the controller will use to render, MUST be a CosmicFramework\MVC view object!
    public $view;

    public function addView($view) {
        $this->view = $view;
        $this->view->setItems([]);
    }
    
    // Renders the view object
    public function render() {
        $this->view->render();
    }
}